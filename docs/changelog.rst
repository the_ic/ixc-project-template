Changelog
=========

0.1 (in development)
--------------------

Document new stuff and fixes here. Use bullet points for brief descriptions of
individual features and fixes, and a paragraphs for a general overview of each
release.

When the list of changes for a release gets sufficiently long (half a page to a
page) or major features or fixes are implemented, cut a release.

New stuff:

* Created project from
  `ixc-project-template <https://github.com/ixc/ixc-project-template>`_
